#![feature(asm_const)]
#![no_std]
#![no_main]

mod bsp;
mod cpu;
mod panic_handler;

use core::arch::asm;

fn kernel_main() -> ! {
	// Turn gpio pin 29 on and off
	unsafe {
		core::ptr::write_volatile(0x3F20_0008 as *mut u32, 1 << 3);

		loop {
			core::ptr::write_volatile(0x3F20_001C as *mut u32, 1 << 21);

			for _ in 1..500000 {
				asm!("nop");
			}

			core::ptr::write_volatile(0x3F20_0028 as *mut u32, 1 << 21);

			for _ in 1..500000 {
				asm!("nop");
			}
		}
	}
}
