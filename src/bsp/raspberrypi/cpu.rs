/// Used by `arch` code to find the early boot core.
#[no_mangle]
#[link_section = ".text._boot_arguments"]
pub static BOOT_CORE_ID: u64 = 0;
