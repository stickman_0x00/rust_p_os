//--------------------------------------------------------------------------------------------------
// Public Code
//--------------------------------------------------------------------------------------------------
.section .text._boot

//------------------------------------------------------------------------------
// fn _boot()
//------------------------------------------------------------------------------
_boot:
	// We want to work only with 1 core, the first one, halt the others.
	mrs     x0, MPIDR_EL1   // Get processor id from MPIDR_EL1 system register.
							// load value from register
	// and     x0, x0, 0xFF    // Check processor id (x0 = x0 & 0xFF)
	// cbz     x0, .init_sp     // If the current process ID is 0, then execution is transferred to the
							; // master function (branch if x0 == 0)
	// b       halt            // Hang for all non-primary CPU
	and	x0, x0, {CONST_CORE_ID_MASK}
	ldr	x1, BOOT_CORE_ID      // provided by bsp/__board_name__/cpu.rs
	cmp	x0, x1
	b.ne	halt


.init_sp: // initialize the stack pointer
	ldr		x5, =_boot
	mov     sp, x5

.clear_bss: // Initialize DRAM.
	ldr     x5, =__bss_start // Start address
	ldr     w6, =__bss_end_exclusive // Size of the section

1:	cbz     w6, 2f // Clean
	str     xzr, [x5], #8
	sub     w6, w6, #1
	cbnz    w6, 1b			// Loop if non-zero

	// pass execution to the _start_rust function
2:	bl      _start_rust
    // In case it does return, halt the master core too

.size	_boot, . - _boot
.type	_boot, function
.global _boot

halt:
	wfe
	b halt

.size	halt, . - halt
.type	halt, function