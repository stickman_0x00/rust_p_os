/// Wait For Event
#[inline(always)]
pub fn wfe() {
	unsafe { core::arch::asm!("wfe", options(nomem, nostack)) }
}
