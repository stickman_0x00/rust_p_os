# RESOURCES

## OSDEV

 - [Raspberry Pi Bare Bones](https://wiki.osdev.org/Raspberry_Pi_Bare_Bones)

## PDF

 - [BCM2837 ARM Peripherals](https://cs140e.sergio.bz/docs/BCM2837-ARM-Peripherals.pdf)

## GIT

 - [MAIN](https://github.com/rust-embedded/rust-raspberrypi-OS-tutorials/)
 - [FIRMWARE](https://github.com/raspberrypi/firmware/tree/master/boot)

## VIDEOS

### Youtube

 - [boot directly in rust](https://www.youtube.com/watch?v=jZT8APrzvc4)